package com.mobimore.server.api;

import com.mobimore.server.api.sobjects.Response;

import java.io.File;
import java.util.Map;

public abstract class Command {
	public abstract Response processCommand(Map<String, String> requestBody, String threadID);
	public abstract ExecRank getRank();
	public abstract String getName();
	public abstract void initialize();
	
	public final Logger getLogger(){
		return new Logger(getName());
	}
	public final static String getDir(){
		return new File(".").getAbsolutePath()+File.separator;
	}
	public final static String getPluginDir(){
		String patch = new File(".").getAbsolutePath();
		return patch + File.separator + "plugins" + File.separator + "CommandPlugin" + File.separator;
	}
}
