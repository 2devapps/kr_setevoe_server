package com.mobimore.server.api.sobjects;

public abstract class Response {
	private String commandName;
	private String type = "text/plain";
	private int httpCode;

	public Response(String commandName, int httpCode){
		this.commandName = commandName;
		this.httpCode = httpCode;
	}

	public String getCommandName() {
		return commandName;
	}
	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}

	public int getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public abstract byte[] getBytes();

	@Override
	public String toString() {
		return "Response{" +
				"commandName='" + commandName + '\'' +
				", type='" + type + '\'' +
				", httpCode=" + httpCode +
				'}';
	}
}
