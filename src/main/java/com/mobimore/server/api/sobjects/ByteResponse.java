package com.mobimore.server.api.sobjects;

import java.util.Arrays;

//Response to clients with bytes
public class ByteResponse extends Response {
    private byte[] bytes = new byte[0];

    public ByteResponse(byte[] bytes, String type, String commandName, int httpCode) {
        super(commandName, httpCode);
        setType(type);
        if (bytes != null) {
            this.bytes = Arrays.copyOf(bytes, bytes.length);
        }
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }
}
