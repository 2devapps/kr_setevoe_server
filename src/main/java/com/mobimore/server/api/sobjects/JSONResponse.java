package com.mobimore.server.api.sobjects;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class JSONResponse extends Response{
	private Map<String, String> responseBody = new HashMap<>();
	private int errorCode = 0;
	/*
	 * @param String responseBody containing responseBody that is sending to client
	 * @param int commandName
	 * @param int httpCode
	 */
	public JSONResponse(Map<String, String> responseBody, String commandName){
		super(commandName, 200);
		this.responseBody = new HashMap<>(responseBody);
		setType("application/json");
	}
	public JSONResponse(Map<String, String> responseBody, String commandName, int errorCode){
		super(commandName, 200);
		this.responseBody = new HashMap<>(responseBody);
		this.errorCode = errorCode;
		setType("application/json");
	}

	public JSONResponse(String message, String commandName) {
		super(commandName, 200);
		responseBody.put("message", message);
		setType("application/json");
	}
	public JSONResponse(String message, String commandName, int errorCode) {
		super(commandName, 200);
		responseBody.put("message", message);
		this.errorCode = errorCode;
		setType("application/json");
	}

	public JSONResponse(String commandName) {
		super(commandName, 200);
		setType("application/json");
	}
	public JSONResponse(String commandName, int errorCode) {
		super(commandName, 200);
		this.errorCode = errorCode;
		setType("application/json");
	}

	public Map<String, String> getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(Map<String, String> responseBody) {
		this.responseBody = new HashMap<>(responseBody);
	}

	public void addResponseEntry(String key, String value) {
		responseBody.put(key, value);
	}
	public String getResponseEntry(String key) {
		return responseBody.get(key);
	}
	public void removeResponseEntry(String key) {
		responseBody.remove(key);
	}

	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public byte[] getBytes() {
		Gson gson = new Gson();
		String responseJSON = gson.toJson(this);
		return responseJSON.getBytes();
	}

	@Override
	public String toString() {
		return "JSONResponse{" +
				"responseBody=" + responseBody +
				'}';
	}
}
