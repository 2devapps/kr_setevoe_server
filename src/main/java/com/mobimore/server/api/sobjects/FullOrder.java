package com.mobimore.server.api.sobjects;

import java.sql.Timestamp;

public class FullOrder extends Order {
    private Timestamp date;
    private Type status;
    private int userID;
    private String id;

    public Timestamp getDate() {
        return date;
    }
    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Type getStatus() {
        return status;
    }
    public void setStatus(Type status) {
        this.status = status;
    }

    public int getUserID() {
        return userID;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public enum Type{
        OPEN, DELIVERED, CANCELLED
    }
}
