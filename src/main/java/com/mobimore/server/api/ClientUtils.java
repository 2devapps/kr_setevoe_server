package com.mobimore.server.api;

import com.mobimore.server.main.UserList;
import com.mobimore.server.api.sobjects.User;

public class ClientUtils {
	private User player = null;
	private String threadName = null;
	/**
	 * 
	 * @param threadName the name of thread that is processing your client
	 */
	public ClientUtils(String threadName){
		this.threadName = threadName;
	}
	/**
	 * Is used to get current user.
	 * @return user that is processed in thread that was send as argument to constructor
	 */
	public User getUser(){
		if(player == null){
			player = UserList.getUser(threadName);
		}
		return player;
	}
}
