package com.mobimore.server.api;

import java.util.Date;

public class Logger {
	String dateS;
	String logLevel;
	String commandName;
	public Logger(String commandName){
		logLevel = "["+"INFO"+"]"+" "+"["+commandName+"]"+" ";
	}
	public void log(String text){
		dateS = new Date().toString();
		System.out.println(dateS+logLevel+text);
	}
}
