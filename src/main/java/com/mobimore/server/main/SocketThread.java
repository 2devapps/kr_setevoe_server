package com.mobimore.server.main;

import com.google.gson.Gson;
import com.mobimore.server.api.Logger;
import com.mobimore.server.api.exceptions.CommandNotFoundException;
import com.mobimore.server.api.exceptions.CommandToLowRankException;
import com.mobimore.server.api.sobjects.*;
import com.mobimore.server.configs.ServerConfiguration;
import com.mobimore.server.utils.DatabaseUtils;
import com.mobimore.server.utils.MD5Hash;
import com.mobimore.server.utils.TokenUtils;

import java.io.*;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;

public class SocketThread extends Thread {
    private Gson gson = new Gson();

    private BufferedReader bufferedReader;
    private PrintWriter printWriter;
    private BufferedOutputStream bufferedOutputStream;

    private Commands commands;
    private String threadToken;
    private Logger logger = new Logger("SERVER");

    //Generate token based on client IP Address
    private String genThreadToken(String clientIPAddress) {
        if (threadToken != null && threadToken.length() > 0) {
            return threadToken;
        }
        StringBuilder sb = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        String loginMD5 = MD5Hash.md5Hash(clientIPAddress);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int milliseconds = calendar.get(Calendar.MILLISECOND);
        sb.append(year)
                .append(month)
                .append(day)
                .append(hour)
                .append(minute)
                .append(seconds)
                .append(milliseconds)
                .append(loginMD5);
        return sb.toString();
    }

    SocketThread(Socket socket) {
        String clientIPAddressWithPort = socket.getRemoteSocketAddress().toString();
        threadToken = genThreadToken(clientIPAddressWithPort);

        this.setName(threadToken);

        if (ServerConfiguration.IS_IN_DEBUG) {
            logger.log("Client connected. Ip address - " + clientIPAddressWithPort);

        }

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printWriter = new PrintWriter(socket.getOutputStream());
            bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
        }

        commands = new Commands(this.getName());
        start();
    }

    @Override
    public void run() {
        try {
            String input = bufferedReader.readLine();

            StringTokenizer parse = new StringTokenizer(input);
            String method = parse.nextToken().toUpperCase();
            String commandAndArguments = parse.nextToken();
            while(commandAndArguments.startsWith("/")) {
                commandAndArguments = commandAndArguments.replaceFirst("/", "");
            }

            if (!method.equals("GET") && !method.equals("HEAD") && !(method.equals("POST"))) {
                //not implemented
            } else {
                String command;
                Map<String, String> requestParams = new HashMap<>();

                if (method.equals("GET") || method.equals("HEAD")) {
                    int argsStartPosition = commandAndArguments.indexOf("?");
                    if (argsStartPosition != -1) { //arguments can be found and parsed
                        command = commandAndArguments.substring(0, argsStartPosition);
                        String args = commandAndArguments.substring(argsStartPosition + 1);
                        parseQuery(args, requestParams);
                    } else { //command without arguments
                        command = commandAndArguments;
                    }
                } else { //method POST
                    command = commandAndArguments;
                    StringBuilder payload = new StringBuilder();
                    while (bufferedReader.ready()) {
                        payload.append((char) bufferedReader.read());
                    }
                    //Parse POST body
                    String[] body = payload.toString().split("\r\n");
                    int bodyEmptyLine = -1;
                    for (int i = 0; i < body.length; i++) {
                        if (body[i].equals("")) {
                            bodyEmptyLine = i;
                        }
                    }
                    if (bodyEmptyLine != -1) {
                        String args = body[bodyEmptyLine + 1];
                        parseQuery(args, requestParams);
                    }
                }

                String userToken = requestParams.getOrDefault("token", "");
                //Token found and can be removed from parameters
                requestParams.remove("token");

                boolean tokenValid = TokenUtils.isUserTokenValid(userToken);
                if (tokenValid) {
                    try {
                        User user = DatabaseUtils.getUserByToken(userToken);
                        if (user != null) { //if user found in database, add to user list
                            UserList.addUser(threadToken, user);
                        }
                    } catch (SQLException ignored) {
                    }
                }

                Request request = new Request(requestParams, command);
                Response response;
                if (commands.commandWithNameExists(command)) {
                    try {
                        response = commands.processCommand(request);
                    } catch (CommandNotFoundException e) {
                        response = new JSONResponse("Request not found", request.getCommandName(), 1);
                    } catch (CommandToLowRankException e) {
                        response = new JSONResponse("You can't execute this request", request.getCommandName(), 3);
                    }
                }else{
                    //if command not found, try to send file from disk
                    command = command.replaceAll("\\.\\.", "");
                    if (command.isEmpty()) {
                        command = ServerConfiguration.DEFAULT_PAGE;
                    }
                    File file = new File(ServerConfiguration.HTTP_SERVER_DIRECTORY, command);
                    if (file.exists()) {
                        String type = "";
                        if (file.getPath().endsWith(".js")) {
                            type = "text/javascript";
                        } else if (file.getPath().endsWith(".css")) {
                            type = "text/css";
                        } else if (file.getPath().endsWith("jpg")) {
                            type = "image/jpeg";
                        }
                        response = new ByteResponse(Files.readAllBytes(file.toPath()), type, command, 200);
                    }else{ //file not found. Error 404
                        byte[] bytes = Files.readAllBytes(Paths.get(ServerConfiguration.HTTP_SERVER_DIRECTORY + File.separator + ServerConfiguration.ERROR_PAGE));
                        response = new ByteResponse(bytes, "text/html", command, 200);
                    }
                }
                sendResponse(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Parse GET and POST HTTP query
    private static void parseQuery(String query, Map<String, String> parameters) throws UnsupportedEncodingException {
        if (query != null) {
            String[] pairs = query.split("[&]");
            for (String pair : pairs) {
                String[] param = pair.split("[=]");
                String key = null;
                String value = null;
                if (param.length > 0) {
                    key = URLDecoder.decode(param[0],
                            System.getProperty("file.encoding"));
                }

                if (param.length > 1) {
                    value = URLDecoder.decode(param[1],
                            System.getProperty("file.encoding"));
                }

				/*if (parameters.containsKey(key)) {
					Object obj = parameters.get(key);
					if (obj instanceof List<?>) {
						List<String> values = (List<String>) obj;
						values.add(value);

					} else if (obj instanceof String) {
						List<String> values = new ArrayList<>();
						values.add((String) obj);
						values.add(value);
						parameters.put(key, values);
					}
				} else {
					parameters.put(key, value);
				}*/
                parameters.put(key, value);
            }
        }
    }

    private void sendResponse(Response response) throws IOException {
        if (response != null) {
            byte[] responseBytes = response.getBytes();

            printWriter.println("HTTP/1.1 " + response.getHttpCode() + " OK");
            printWriter.println("Server: "+ ServerConfiguration.SERVER_NAME);
            printWriter.println("Date: " + new Date());
            printWriter.println("Content-type: " + response.getType());
            printWriter.println("Content-length: " + responseBytes.length);
            printWriter.println(); // blank line between headers and content, very important !
            printWriter.flush(); // flush character output stream buffer

            bufferedOutputStream.write(responseBytes);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
        }else{
            printWriter.println("HTTP/1.1 200 OK");
            printWriter.println("Server: "+ ServerConfiguration.SERVER_NAME);
            printWriter.println("Date: " + new Date());
            printWriter.println(); // blank line between headers and content, very important !
            printWriter.flush(); // flush character output stream buffer

            bufferedOutputStream.close();
        }
    }
}
