package com.mobimore.server.main;

import com.mobimore.server.api.sobjects.User;

import java.util.HashMap;

public class UserList {
	private static HashMap<String, User> usersMap = new HashMap<>();
	static void addUser(String threadToken, User user){
		if(!usersMap.containsValue(user)){
			usersMap.put(threadToken, user);
		}
	}
	public static User getUser(String threadName){
		return usersMap.get(threadName);
	}
	static void removeUser(String threadName){
		usersMap.remove(threadName);
	}
}
