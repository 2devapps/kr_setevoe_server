package com.mobimore.server.main;

import com.mobimore.server.api.Command;
import com.mobimore.server.api.Logger;
import com.mobimore.server.api.exceptions.NoCommandsFoundException;
import com.mobimore.server.api.exceptions.PluginsAlreadyLoadedException;
import com.mobimore.server.configs.ServerConfiguration;
import org.reflections.Reflections;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

class PluginsUtils {
	private static int count = 0;
	private static Set<Command> commandArrayList = new HashSet<>();
	private static Logger logger = new Logger("SERVER");

	private static Set<Command> initDefaultPlugins(){
		Reflections reflections = new Reflections("com.mobimore.server.defcommands");

		Set<Class<? extends Command>> allCommandsClasses = reflections.getSubTypesOf(Command.class);

		Set<Command> commands = new HashSet<>();
		allCommandsClasses.forEach(e->{
			try {
				commands.add(e.getConstructor().newInstance());
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ex) {
				logger.log("Error loading command "+ e.getName());
			}
		});
		return commands;
	}

	//load all commands from "plugins" folder and package "com.mobimore.server.defcommands"
	static void loadPlugins() throws PluginsAlreadyLoadedException, NoCommandsFoundException{
		if(count != 0){
			throw new PluginsAlreadyLoadedException();
		}
		commandArrayList.addAll(initDefaultPlugins());
		File path = new File("."+File.separator+"plugins");
		if(!path.exists()){ 
			if(!commandArrayList.isEmpty()){
				for(Command command:commandArrayList){
					initializeCommand(command);
				}
				return;
			}
			throw new NoCommandsFoundException();
		}
		File[] jars = path.listFiles(arg0 -> arg0.isFile() && arg0.getName().endsWith(".jar"));
		@SuppressWarnings("rawtypes")
		Class[] plugins = new Class[jars.length];
		for(int i = 0;i<jars.length;i++){
			try {
				@SuppressWarnings("resource")
				URLClassLoader uCL = new URLClassLoader(new URL[]{jars[i].toURI().toURL()});
				JarFile file = new JarFile(jars[i]);
				Manifest manifest = file.getManifest();
				Attributes attr = manifest.getMainAttributes();
				String commandClass = attr.getValue("CLASS");
				file.close();
				if(commandClass == null){
					continue;
				}
				plugins[i] = uCL.loadClass(commandClass);
				Command command = (Command) plugins[i].getConstructor().newInstance();
				commandArrayList.add(command);
			} catch (InstantiationException | IOException | IllegalAccessException | NoSuchMethodException |
					ClassNotFoundException | InvocationTargetException e) {
				logger.log("Error loading file " + jars[i].getName() + " wrong Request class specefied in Manifest.mf");
			}
		}
		for(Command command:commandArrayList){
			initializeCommand(command);
		}
		count++;
	}
	private static void initializeCommand(Command command){
		if(ServerConfiguration.IS_IN_DEBUG){
			logger.log("Loading plugin: " + command.getName() + "...");
		}else{
			logger.log("Loading plugin: " + command.getName() + "...");
		}
		command.initialize();
		logger.log("Plugin " + command.getName() + " loaded.");
	}
	static Set<Command> getLoadedPlugins(){
		return commandArrayList;
	}
}
