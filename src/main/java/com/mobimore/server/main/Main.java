package com.mobimore.server.main;

import com.mobimore.server.configs.ServerConfiguration;
import com.mobimore.server.api.Logger;
import com.mobimore.server.api.exceptions.NoCommandsFoundException;
import com.mobimore.server.api.exceptions.PluginsAlreadyLoadedException;

import java.io.IOException;
import java.net.ServerSocket;

public class Main {
	private final static boolean LISTENING = true;
	private static Logger logger = new Logger("SERVER");

	//@SuppressWarnings("unused")
	public static void main(String[] args){
		try {
			if(ServerConfiguration.IS_IN_DEBUG){
				System.out.println("!!!THIS APPLICATION IS IN DEBUG MODE!!!");
			}
			System.out.println("Starting server " + ServerConfiguration.SERVER_NAME + "...");
			logger.log("Opening socket...");
			@SuppressWarnings("resource")
			ServerSocket sSocket = new ServerSocket(ServerConfiguration.SERVER_PORT);
			logger.log("Socket opened...");
			logger.log("Loading plugins...");
			PluginsUtils.loadPlugins();
			logger.log("Plugins loaded...");
			logger.log("Connections allowed on port " + ServerConfiguration.SERVER_PORT + ".");
			while(LISTENING){
				 new SocketThread(sSocket.accept());
			}
		} catch (IOException | PluginsAlreadyLoadedException e) {
			e.printStackTrace();
		} catch (NoCommandsFoundException e) {
			logger.log("No commands found, start failed...");
		}

	}
}
