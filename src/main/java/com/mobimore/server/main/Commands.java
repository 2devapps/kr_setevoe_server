package com.mobimore.server.main;

import com.mobimore.server.api.sobjects.Request;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.api.ClientUtils;
import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.exceptions.CommandNotFoundException;
import com.mobimore.server.api.exceptions.CommandToLowRankException;
import com.mobimore.server.api.sobjects.User;

import java.util.HashMap;
import java.util.Set;

class Commands {
	private final ClientUtils clientUtils;
	private String threadName;
	private HashMap<String, Command> commandNamesMap = new HashMap<>();

	Commands(String threadName) {
		this.clientUtils = new ClientUtils(threadName);
		this.threadName = threadName;
		Set<Command> commandsArrayList = PluginsUtils.getLoadedPlugins();
		for (Command command : commandsArrayList) {
			commandNamesMap.put(command.getName(), command);
		}
	}

	public boolean commandWithNameExists(String name){
		return commandNamesMap.containsKey(name);
	}

	private ExecRank getCommandExecRank(String command){
		return commandNamesMap.get(command).getRank();
	}

	private boolean canExecuteCommand(String command) {
		if (!commandWithNameExists(command)) return false;
		User user = clientUtils.getUser();
		ExecRank userRank = ExecRank.NAU;
		if (user != null) {
			userRank = user.getRank();
		}
		return userRank.compareTo(getCommandExecRank(command)) >= 0;

	}

	Response processCommand(Request requestFromClient) throws CommandNotFoundException, CommandToLowRankException {
		Command command = commandNamesMap.get(requestFromClient.getCommandName());
		if (command == null) {
			throw new CommandNotFoundException();
		}
		if (canExecuteCommand(requestFromClient.getCommandName())) {
			return command.processCommand(requestFromClient.getRequestBody(), threadName);
		}
		throw new CommandToLowRankException();
	}
}
