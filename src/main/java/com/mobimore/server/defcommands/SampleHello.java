package com.mobimore.server.defcommands;

import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Response;

import java.util.HashMap;
import java.util.Map;

public class SampleHello extends Command {

    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("message", "Hello World");
        return new JSONResponse(responseBody, getName(), 0);
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.USER;
    }

    @Override
    public String getName() {
        return "SimpleHello";
    }

    @Override
    public void initialize() {

    }
}
