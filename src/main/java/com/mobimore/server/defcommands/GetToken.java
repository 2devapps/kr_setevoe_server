package com.mobimore.server.defcommands;

import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.api.sobjects.User;
import com.mobimore.server.configs.ServerConfiguration;
import com.mobimore.server.utils.DatabaseUtils;
import com.mobimore.server.utils.MD5Hash;
import com.mobimore.server.utils.TokenUtils;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GetToken extends Command {

    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        try {
            String login = requestBody.get("login");
            String password = requestBody.get("password");
            if (login != null && !login.equals("") && password != null && !password.equals("")) {
                User user = DatabaseUtils.getUserByLogin(login);
                if (user != null) {
                    String passwordMD5 = MD5Hash.md5Hash(password);
                    if (user.getPassword().equalsIgnoreCase(passwordMD5)) {
                        if (!TokenUtils.isUserTokenValid(user.getAuthToken())) {
                            Date currentDate = new Date();
                            SimpleDateFormat dateWithTime = new SimpleDateFormat("MMddyyy:hhmmssSSS");
                            SimpleDateFormat dateOnly = new SimpleDateFormat("MMddyyy");
                            user.setAuthToken(MD5Hash.md5Hash(login + "+" + password + dateWithTime.format(currentDate)) + dateOnly.format(currentDate));
                            DatabaseUtils.updateUserTokenByID(user.getId(), user.getAuthToken());
                        }
                        HashMap<String, String> resultMessage = new HashMap<>();
                        resultMessage.put("message", "Authorization OK");
                        resultMessage.put("token", user.getAuthToken());
                        return new JSONResponse(resultMessage, getName(), 0);
                    }
                }
            }

        } catch (SQLException e) {
            if(ServerConfiguration.IS_IN_DEBUG){
                e.printStackTrace();
            }
            return new JSONResponse("Database error", getName(),1);
        }
        return new JSONResponse("Login or password incorrect", getName(),2);
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.NAU;
    }

    @Override
    public String getName() {
        return "getToken";
    }

    @Override
    public void initialize() {

    }
}
