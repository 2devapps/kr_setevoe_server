package com.mobimore.server.defcommands;

import com.google.gson.Gson;
import com.mobimore.server.api.ClientUtils;
import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.api.sobjects.User;

import java.util.Map;

public class GetUser extends Command {
    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        Gson gson = new Gson();
        ClientUtils clientUtils = new ClientUtils(threadID);
        User user = clientUtils.getUser();
        assert user == null;
        //get by id
        JSONResponse response = new JSONResponse(getName(), 0);
        response.addResponseEntry("user", gson.toJson(user));
        return response;
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.USER;
    }

    @Override
    public String getName() {
        return "getUser";
    }

    @Override
    public void initialize() {

    }
}
