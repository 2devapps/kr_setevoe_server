package com.mobimore.server.defcommands;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Product;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.utils.DatabaseUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetProducts extends Command {

    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        String action = requestBody.getOrDefault("action", "");
        Map<String, String> response = new HashMap<>();
        response.put("action", action);
        switch (action) {
            case "count":
                int productsCount = DatabaseUtils.getProductsCountFromDB();
                response.put("count", Integer.toString(productsCount));
                return new JSONResponse(response, getName(), 0);
            case "get":
                String offsetStr = requestBody.get("offset");
                String countStr = requestBody.get("count");

                if (offsetStr != null && !offsetStr.isEmpty() && countStr != null && !countStr.isEmpty()) {
                    try {
                        int offset = Integer.parseInt(offsetStr);
                        int count = Integer.parseInt(countStr);
                        Gson gson = new Gson();
                        List<Product> products = DatabaseUtils.getProductsFromDB(offset, count);
                        response.put("products", gson.toJson(products));
                        return new JSONResponse(response, getName(), 0);
                    } catch (NumberFormatException e) {
                        return new JSONResponse("Bad request", getName(), 1);
                    }
                } else {
                    return new JSONResponse("Bad request", getName(), 1);
                }
            case "getAll":
                try {
                    Gson gson = new Gson();
                    List<Product> products = DatabaseUtils.getAllProductsFromDB();
                    response.put("products", gson.toJson(products));
                    return new JSONResponse(response, getName(), 0);
                } catch (NumberFormatException e) {
                    return new JSONResponse("Bad request", getName(), 1);
                }
            case "getByIDs":
                Gson gson = new Gson();
                String idsListJSON = requestBody.get("ids");
                if (idsListJSON != null && !idsListJSON.isEmpty()) {
                    Type founderListType = new TypeToken<ArrayList<Integer>>(){}.getType();
                    List<Integer> ids = gson.fromJson(idsListJSON, founderListType);

                    Map<Integer, Product> products = DatabaseUtils.getProductFromDB(ids.toArray(new Integer[0]));
                    response.put("products", gson.toJson(products));
                    return new JSONResponse(response, getName(), 0);
                }else{
                    return new JSONResponse("Bad request", getName(), 1);
                }
            default:
                return new JSONResponse("Bad request", getName(), 1);
        }
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.NAU;
    }

    @Override
    public String getName() {
        return "getProducts";
    }

    @Override
    public void initialize() {

    }
}
