package com.mobimore.server.defcommands;

import com.google.gson.Gson;
import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.*;
import com.mobimore.server.configs.ServerConfiguration;
import com.mobimore.server.utils.DatabaseUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetOrderDetails extends Command {
    private Gson gson = new Gson();
    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        String orderID = requestBody.get("id");
        if (orderID != null && !orderID.isEmpty()) {
            FullOrder order = DatabaseUtils.getOrderByID(orderID);
            if (order != null) {
                Integer[] arr = order.getProductAndCountMap().keySet().toArray(new Integer[0]);
                Map<Integer, Product> productList = DatabaseUtils.getProductFromDB(arr);
                if (productList == null) {
                    return new JSONResponse("Order downloading error", getName(), 2);
                }
                try {
                    User user = DatabaseUtils.getUserByID(order.getUserID());
                    HashMap<String, String> response = new HashMap<>();
                    response.put("order", gson.toJson(order));
                    response.put("productsMap", gson.toJson(productList));
                    response.put("user", gson.toJson(user));
                    return new JSONResponse(response, getName(), 0);
                } catch (SQLException e) {
                    if (ServerConfiguration.IS_IN_DEBUG) {
                        e.printStackTrace();
                    }
                    return new JSONResponse("Order downloading error", getName(), 2);
                }
            }else{
                return new JSONResponse("Order not found", getName(), 2);
            }
        }else {
            return new JSONResponse("Bad request", getName(), 1);
        }
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.SPEC;
    }

    @Override
    public String getName() {
        return "getOrderDetails";
    }

    @Override
    public void initialize() {

    }


}
