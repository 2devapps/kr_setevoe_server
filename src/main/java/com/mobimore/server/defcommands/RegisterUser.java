package com.mobimore.server.defcommands;

import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.api.sobjects.User;
import com.mobimore.server.configs.ServerConfiguration;
import com.mobimore.server.utils.DatabaseUtils;
import com.mobimore.server.utils.MD5Hash;

import java.sql.SQLException;
import java.util.Map;

public class RegisterUser extends Command {

    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        try {
            String login = requestBody.get("login");
            String password = requestBody.get("password");
            String name = requestBody.get("name");
            if (login != null && !login.equals("") && password != null && !password.equals("") && name != null && !name.equals("")) {
                User user = DatabaseUtils.getUserByLogin(login);
                if (user == null) { //user not found, safe to register new user
                    String passwordMD5 = MD5Hash.md5Hash(password);
                    //add new user
                    DatabaseUtils.insertUser(login, passwordMD5, name);
                    return new JSONResponse("Registration OK", getName(), 0);
                }else{
                    return new JSONResponse("Registration failed. User with login " + login + " already exists", getName(), 1);
                }
            }

        } catch (SQLException e) {
            if(ServerConfiguration.IS_IN_DEBUG){
                e.printStackTrace();
            }
            return new JSONResponse("Database error", getName(), 2);
        }

        return new JSONResponse("Registration failed", getName(), 3);
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.NAU;
    }

    @Override
    public String getName() {
        return "register";
    }

    @Override
    public void initialize() {

    }
}
