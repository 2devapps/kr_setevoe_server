package com.mobimore.server.defcommands;

import com.google.gson.Gson;
import com.mobimore.server.api.ClientUtils;
import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Order;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.api.sobjects.User;
import com.mobimore.server.configs.DatabaseConfiguration;
import com.mobimore.server.utils.MD5Hash;

import java.sql.*;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PlaceOrder extends Command {
    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        ClientUtils clientUtils = new ClientUtils(threadID);
        User user = clientUtils.getUser();
        String orderJSON = requestBody.get("order");
        if (orderJSON != null && !orderJSON.isEmpty()) {
            Gson gson = new Gson();
            Order order = gson.fromJson(orderJSON, Order.class);
            if (order.getDeliveryAddress() == null || order.getDeliveryAddress().isEmpty()) {
                return new JSONResponse("Error placing your order", getName(), 2);
            }
            try {
                if (insertOrder(order, user)) {
                    return new JSONResponse("Your order placed successfully", getName(), 0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return new JSONResponse("Error placing your order", getName(), 1);
            }
        }
        return new JSONResponse("Error placing your order", getName(), 1);
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.USER;
    }

    @Override
    public String getName() {
        return "placeOrder";
    }

    @Override
    public void initialize() {

    }

    public static boolean insertOrder(Order order, User user) throws SQLException {
        boolean ok = true;
        Connection conn = DriverManager.getConnection("jdbc:mysql://"+ DatabaseConfiguration.DB_HOST+"/"+DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
        HashMap<Integer, Integer> orderData = order.getProductAndCountMap();
        Timestamp timestamp = new Timestamp(new Date().getTime());
        StringBuilder sb = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int milliseconds = calendar.get(Calendar.MILLISECOND);
        sb.append(year)
                .append(month)
                .append(day)
                .append(hour)
                .append(minute)
                .append(seconds)
                .append(milliseconds)
                .append(user.getLogin());

        for (Integer productID : orderData.keySet()) {
            PreparedStatement pst = conn.prepareStatement("INSERT INTO "+DatabaseConfiguration.DB_ORDERS_TABLE_NAME+" (userID, place, productID, count, paymentType, datetime, id2) VALUES (?,?,?,?,?,?,?)");
            pst.setInt(1, user.getId());
            pst.setString(2, order.getDeliveryAddress());
            pst.setInt(3, productID);
            pst.setInt(4, orderData.get(productID));
            pst.setString(5, order.getPaymentType().name());

            pst.setTimestamp(6, timestamp);

            pst.setString(7, sb.toString());
            if (pst.executeUpdate() != 1) {
                ok = false;
            }
            pst.close();
        }
        conn.close();
        return ok;
    }
}
