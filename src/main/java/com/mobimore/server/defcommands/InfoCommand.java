package com.mobimore.server.defcommands;

import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Response;

import java.util.Map;

public class InfoCommand extends Command {

	@Override
	public ExecRank getRank() {
		return ExecRank.SPEC;
	}

	@Override
	public Response processCommand(Map<String, String> requestBody, String threadName) {
		return new JSONResponse("Server running. " + "Directory: " + getDir(), getName(), 0);
	}

	@Override
	public String getName() {
		return "infoC";
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}
}
