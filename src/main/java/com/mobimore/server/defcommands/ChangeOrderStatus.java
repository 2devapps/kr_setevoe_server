package com.mobimore.server.defcommands;

import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.FullOrder;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.utils.DatabaseUtils;

import java.util.Map;

public class ChangeOrderStatus extends Command {
    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        String orderID = requestBody.get("id");
        String status = requestBody.get("status");
        if (orderID != null && !orderID.isEmpty() && status != null && !status.isEmpty()) {
            try {
                FullOrder.Type orderType = FullOrder.Type.valueOf(status);
                boolean ok = DatabaseUtils.modifyOrderStatus(orderID, orderType);
                if (ok) {
                    return new JSONResponse("Status modified successfully", getName(), 0);
                }else{
                    return new JSONResponse("Order id not found", getName(), 3);
                }
            } catch (IllegalArgumentException e) {
                return new JSONResponse("Status not recognized", getName(), 2);
            }
        }else{
            return new JSONResponse("Bad request", getName(), 1);
        }
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.SPEC;
    }

    @Override
    public String getName() {
        return "changeOrderStatus";
    }

    @Override
    public void initialize() {

    }
}
