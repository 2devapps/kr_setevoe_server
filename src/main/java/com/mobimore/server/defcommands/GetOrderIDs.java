package com.mobimore.server.defcommands;

import com.google.gson.Gson;
import com.mobimore.server.api.Command;
import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.FullOrder;
import com.mobimore.server.api.sobjects.JSONResponse;
import com.mobimore.server.api.sobjects.Response;
import com.mobimore.server.configs.DatabaseConfiguration;
import com.mobimore.server.configs.ServerConfiguration;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetOrderIDs extends Command {
    private Gson gson = new Gson();

    @Override
    public Response processCommand(Map<String, String> requestBody, String threadID) {
        String type = requestBody.get("status");
        if (type != null && !type.isEmpty()) {
            if (type.equalsIgnoreCase("all")) {
                List<String> ids = selectOrderIDsFromDB();
                HashMap<String, String> response = new HashMap<>();
                response.put("orderIDs", gson.toJson(ids));
                return new JSONResponse(response, getName(), 0);

            } else if (type.equalsIgnoreCase("delivered")) {
                List<String> ids = selectOrderIDsFromDB(FullOrder.Type.DELIVERED);
                HashMap<String, String> response = new HashMap<>();
                response.put("orderIDs", gson.toJson(ids));
                return new JSONResponse(response, getName(), 0);
            } else if (type.equalsIgnoreCase("open")) {
                List<String> ids = selectOrderIDsFromDB(FullOrder.Type.OPEN);
                HashMap<String, String> response = new HashMap<>();
                response.put("orderIDs", gson.toJson(ids));
                return new JSONResponse(response, getName(), 0);
            }
        }
        return new JSONResponse("Bad request", getName(), 1);
    }

    @Override
    public ExecRank getRank() {
        return ExecRank.SPEC;
    }

    @Override
    public String getName() {
        return "getOrderIDs";
    }

    @Override
    public void initialize() {

    }

    private List<String> selectOrderIDsFromDB(FullOrder.Type type) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
            PreparedStatement pst = conn.prepareStatement("SELECT DISTINCT id2 FROM " + DatabaseConfiguration.DB_ORDERS_TABLE_NAME +" WHERE status = ?");
            pst.setString(1, type.name());

            ResultSet rs = pst.executeQuery();

            List<String> orderIDs = new ArrayList<>();
            while (rs.next()) {
                orderIDs.add(rs.getString(1));
            }

            rs.close();
            pst.close();
            conn.close();
            return orderIDs;

        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private List<String> selectOrderIDsFromDB() {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
            PreparedStatement pst = conn.prepareStatement("SELECT DISTINCT id2 FROM " + DatabaseConfiguration.DB_ORDERS_TABLE_NAME);

            ResultSet rs = pst.executeQuery();

            List<String> orderIDs = new ArrayList<>();
            while (rs.next()) {
                orderIDs.add(rs.getString(1));
            }

            rs.close();
            pst.close();
            conn.close();
            return orderIDs;

        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
