package com.mobimore.server.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Hash {
	public static String md5Hash(String source){
        byte[] hash = md5Hash(source.getBytes());
        if (hash != null) {
            return String.format("%032X", new BigInteger(1, hash));
            //return new BigInteger(1, hash).toString(16);
        }
        return "";
	}

    public static byte[] md5Hash(byte[] source) {
        MessageDigest m;
        try {
            m = MessageDigest.getInstance("MD5");
            m.update(source, 0, source.length);
            return m.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
