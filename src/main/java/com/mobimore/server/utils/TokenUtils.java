package com.mobimore.server.utils;

import com.mobimore.server.configs.ServerConfiguration;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TokenUtils {
    public static boolean isUserTokenValid(String userToken) {
        if (((userToken != null) && (!userToken.equals("")) && userToken.length() == 40)) {
            String tokenCreationDateString = userToken.substring(userToken.length() - 8); //date format MMDDYY
            try {
                Date tokenCreationDate = new SimpleDateFormat("MMddyyyy").parse(tokenCreationDateString);
                Date currentDate = new Date();

                if (tokenCreationDate.before(currentDate)) {
                    //token was created less than TOKEN_VALID_DAYS ago
                    if (Days.daysBetween(new DateTime(currentDate), new DateTime(tokenCreationDate)).getDays() <= ServerConfiguration.TOKEN_VALID_DAYS) {
                        return true;
                    }
                }
            } catch (ParseException e) {
                return false;
            }
        }
        return false;
    }
}
