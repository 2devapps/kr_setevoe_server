package com.mobimore.server.utils;

import com.mobimore.server.api.ExecRank;
import com.mobimore.server.api.sobjects.FullOrder;
import com.mobimore.server.api.sobjects.Order;
import com.mobimore.server.api.sobjects.Product;
import com.mobimore.server.api.sobjects.User;
import com.mobimore.server.configs.DatabaseConfiguration;
import com.mobimore.server.configs.ServerConfiguration;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseUtils {
	public static User getUserByToken(String token) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:mysql://"+DatabaseConfiguration.DB_HOST+"/"+DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
		PreparedStatement pst = conn.prepareStatement("SELECT * FROM "+DatabaseConfiguration.DB_USERS_TABLE_NAME+" WHERE token = ?");
		pst.setString(1, token);
		pst.setMaxRows(1);
		ResultSet rs = pst.executeQuery();
		if(rs.next()){
            int id = rs.getInt(1);
            String name = rs.getString(2);
            String login = rs.getString(3);
            String password = rs.getString(4);
            int rank = rs.getInt(5);

            User user = new User();
            user.setId(id);
            user.setName(name);
            user.setLogin(login);
            user.setPassword(password);
            user.setRank(ExecRank.values()[rank]);
            user.setAuthToken(token);

            rs.close();
			pst.close();
			conn.close();
			return user;
		}
		rs.close();
		pst.close();
		conn.close();
		return null;
	}
	public static User getUserByLogin(String login) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:mysql://"+DatabaseConfiguration.DB_HOST+"/"+DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
		PreparedStatement pst = conn.prepareStatement("SELECT * FROM "+DatabaseConfiguration.DB_USERS_TABLE_NAME+" WHERE login = ?");
		pst.setString(1, login);
		pst.setMaxRows(1);
		ResultSet rs = pst.executeQuery();
		if(rs.next()){
			int id = rs.getInt(1);
			String name = rs.getString(2);
			String password = rs.getString(4);
			int rank = rs.getInt(5);
			String token = rs.getString(6);

			User user = new User();
			user.setId(id);
			user.setName(name);
			user.setLogin(login);
			user.setPassword(password);
			user.setRank(ExecRank.values()[rank]);
			user.setAuthToken(token);

			rs.close();
			pst.close();
			conn.close();
			return user;
		}

		rs.close();
		pst.close();
		conn.close();
		return null;
	}
    public static User getUserByID(int id) throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:mysql://"+DatabaseConfiguration.DB_HOST+"/"+DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
        PreparedStatement pst = conn.prepareStatement("SELECT * FROM "+DatabaseConfiguration.DB_USERS_TABLE_NAME+" WHERE id = ?");
        pst.setInt(1, id);
        pst.setMaxRows(1);
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            String name = rs.getString(2);
            String login = rs.getString(3);

            User user = new User();
            user.setId(id);
            user.setName(name);
            user.setLogin(login);

            rs.close();
            pst.close();
            conn.close();
            return user;
        }

        rs.close();
        pst.close();
        conn.close();
        return null;
    }

	public static void updateUserTokenByID(int id, String token) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:mysql://"+DatabaseConfiguration.DB_HOST+"/"+DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
		PreparedStatement pst = conn.prepareStatement("UPDATE "+DatabaseConfiguration.DB_USERS_TABLE_NAME+" SET token = ? WHERE id = ?");
		pst.setString(1, token);
		pst.setInt(2, id);
		pst.executeUpdate();
		pst.close();
		conn.close();
	}

	public static void insertUser(String login, String password, String name) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:mysql://"+DatabaseConfiguration.DB_HOST+"/"+DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
		PreparedStatement pst = conn.prepareStatement("INSERT INTO "+DatabaseConfiguration.DB_USERS_TABLE_NAME+" (name, login, password) VALUES (?,?,?)");
		pst.setString(1, name);
		pst.setString(2, login);
		pst.setString(3, password);
		pst.executeUpdate();
		pst.close();
		conn.close();
	}

	public static boolean testDBConnection(){
		try (Connection ignored = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/"
				+ DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD)) {
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

    public static int getProductsCountFromDB() {

        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
            PreparedStatement pst = conn.prepareStatement("SELECT COUNT(id) FROM " + DatabaseConfiguration.DB_PRODUCTS_TABLE);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            rs.close();
            pst.close();
            conn.close();
        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
            return -1;
        }
        return -1;
    }

    public static List<Product> getProductsFromDB(int offset, int count) {
        try {
            int productsCount = getProductsCountFromDB();
            if (productsCount != -1 && productsCount != 0) {
                if (offset >= 0 && count > 0 && offset + count <= productsCount) {
                    Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
                    PreparedStatement pst = conn.prepareStatement("SELECT * FROM " + DatabaseConfiguration.DB_PRODUCTS_TABLE + " LIMIT ?, ?");
                    pst.setInt(1, offset);
                    pst.setInt(2, count);
                    ResultSet rs = pst.executeQuery();

                    List<Product> productList = new ArrayList<>();
                    while (rs.next()) {
                        int id = rs.getInt(1);
                        String name = rs.getString(2);
                        String description = rs.getString(3);
                        String type = rs.getString(4);
                        int price = rs.getInt(5);
                        String imageURL = rs.getString(6);
                        productList.add(new Product(id, name, description, type, price, imageURL));
                    }

                    rs.close();
                    pst.close();
                    conn.close();
                    return productList;
                }
            }

        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
            return null;
        }
        return null;
    }
    public static Map<Integer, Product> getProductFromDB(Integer... ids) {
        try {
            int productsCount = getProductsCountFromDB();
            if (productsCount != -1 && productsCount != 0) {
                Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);

                StringBuilder sqlString = new StringBuilder();
                sqlString.append("SELECT * FROM " + DatabaseConfiguration.DB_PRODUCTS_TABLE + " WHERE");
                for (int i = 0; i < ids.length; i++) {
                    Integer id = ids[i];
                    sqlString.append(" id = ").append(id);
                    if (i != ids.length - 1) {
                        sqlString.append(" OR");
                    }
                }

                PreparedStatement pst = conn.prepareStatement(sqlString.toString());
                ResultSet rs = pst.executeQuery();

                Map<Integer, Product> productsMap = new HashMap<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String description = rs.getString(3);
                    String type = rs.getString(4);
                    int price = rs.getInt(5);
                    String imageURL = rs.getString(6);
                    productsMap.put(id, new Product(id, name, description, type, price, imageURL));
                }

                rs.close();
                pst.close();
                conn.close();
                return productsMap;
            }

        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
            return null;
        }
        return null;
    }
    public static List<Product> getAllProductsFromDB() {
        try {
            int productsCount = getProductsCountFromDB();
            if (productsCount != -1 && productsCount != 0) {
                Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
                PreparedStatement pst = conn.prepareStatement("SELECT * FROM " + DatabaseConfiguration.DB_PRODUCTS_TABLE);
                ResultSet rs = pst.executeQuery();

                List<Product> productList = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String description = rs.getString(3);
                    String type = rs.getString(4);
                    int price = rs.getInt(5);
                    String imageURL = rs.getString(6);
                    productList.add(new Product(id, name, description, type, price, imageURL));
                }

                rs.close();
                pst.close();
                conn.close();
                return productList;
            }

        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
            return null;
        }
        return null;
    }

    public static FullOrder getOrderByID(String id){
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
            PreparedStatement pst = conn.prepareStatement("SELECT * FROM " + DatabaseConfiguration.DB_ORDERS_TABLE_NAME + " WHERE id2 = ?");
            pst.setString(1, id);
            ResultSet rs = pst.executeQuery();

            FullOrder fullOrder = new FullOrder();

            boolean firstRun = true;
            while (rs.next()) {
                if (firstRun) {
                    int userID = rs.getInt(3);
                    String place = rs.getString(4);
                    String paymentType = rs.getString(7);
                    String orderStatus = rs.getString(8);
                    Timestamp timestamp = rs.getTimestamp(9);

                    fullOrder.setId(id);
                    fullOrder.setUserID(userID);
                    fullOrder.setDeliveryAddress(place);
                    fullOrder.setPaymentType(Order.PaymentType.valueOf(paymentType));
                    fullOrder.setStatus(FullOrder.Type.valueOf(orderStatus));
                    fullOrder.setDate(timestamp);
                    firstRun = false;
                }

                int productID = rs.getInt(5);
                int count = rs.getInt(6);

                fullOrder.addProduct(productID, count);
            }

            rs.close();
            pst.close();
            conn.close();

            return fullOrder;
        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
        }
        return null;
	}

    public static boolean modifyOrderStatus(String orderID, FullOrder.Type status) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://" + DatabaseConfiguration.DB_HOST + "/" + DatabaseConfiguration.DB_NAME, DatabaseConfiguration.DB_LOGIN, DatabaseConfiguration.DB_PASSWORD);
            PreparedStatement pst = conn.prepareStatement("UPDATE "+DatabaseConfiguration.DB_ORDERS_TABLE_NAME+" SET status = ? WHERE id2 = ?");
            pst.setString(1, status.name());
            pst.setString(2, orderID);
            int result = pst.executeUpdate();
            pst.close();
            conn.close();
            return result > 0;
        } catch (SQLException e) {
            if (ServerConfiguration.IS_IN_DEBUG) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
