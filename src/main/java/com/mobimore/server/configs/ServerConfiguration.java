package com.mobimore.server.configs;

public class ServerConfiguration {
	public static final String SERVER_NAME = "Mobimore Server Application";
	public static final int SERVER_PORT = 1050;
	public static final int TOKEN_VALID_DAYS = 10;
	public static final String HTTP_SERVER_DIRECTORY = "html";
	public static final String DEFAULT_PAGE = "index.html";
	public static final String ERROR_PAGE = "404.html";
	public static final boolean IS_IN_DEBUG = true;
}
